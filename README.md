# hfv_evol

## README

### Description

The code hfv_evol is an implementation of different Hybrid Finite Volume schemes for linear advection-diffusion equations.

This code was developped in order to investigate the interest of using nonlinear schemes to discretise dissipative problems on general meshes.
It computes a numerical approximation of a transcient anisotropic advection-diffusion equation.
Three schemes are implemented:
* the classical (linear) HMM scheme introduced in [this paper](https://hal.science/hal-00808695);
* a linear scheme based on the [exponential fitting strategy](https://epubs.siam.org/doi/10.1137/0726078);
* a nonlinear scheme which follows the ideas of the VAG scheme introduced in [this paper](https://hal.science/hal-01119735).

The nonlinear discretisation used ensure that the Boltzmann entropy structure of the continuous problem is conserved at the discrete level, and therefore ensure (among others features) that the computed solution is positive.

This code was used to get some numerical results presented in an [article](https://hal.science/hal-03281500) about long-time behaviour and reliability of HFV schemes. <br>
It especially highlights the lack of positivity of the linear methods.

While implemented on a linear problem, this scheme is intended to be used as a basis for methods on more complicated problems, including semiconductor models and cross-diffusion systems. 

#### Installation
 
The following softwares/libraries are required for compilation:

* C++
* CMake
* Eigen
* Boost 

To compile the code, you should create a build directory.


mkdir build                 <br>
cd build                    <br>
cmake ..                    <br>
make                        

The compilation will create a hfv directory, in which you can find and execute "hfv_advdiff_evol".
In order to get some data about visualisation, you should create an "output" directory in the build directory.

mkdir output               <br>
./bin/hfv_advdiff_evol      <br>


### Getting started


The data of the problem (initial condition, source term, boundary values, physical data) can be specified in the ExactSolution constructor (file data.h).
Some specific test case already available in this file, and are commented.<br>
The execution of a scheme generates data about the long-time behaviour (evolution of the entropy), computes the time-space error (only relevant if the test case has an explicit exact solution given in the data) and gives information about the (non-)positivity of the solution.

The visualisation files (.vtu format) can be read directcly using Paraview. 

Note that the "omega scheme" (function "omegaschema" of the code) correspond to a scheme which encompasses the HMM and the exponential fitting scheme. 
The first argument should be equal to 0 for the HMM method, and 1 for the exponential fitting.  

### Authors 

Author: Julien Moatti (julien.moatti@tuwien.ac.at)

### License

GNU General Public License v3.0

### Project status

This has been created for personnal use. There might or might not be updates in the future.