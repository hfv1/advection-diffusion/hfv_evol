// -*- C++ -*-
#ifndef AFFINETRANSFORMATION_H
#define AFFINETRANSFORMATION_h

#include "Common/defs.h"
#include "ITransformation.h"

class AffineTransformation
  : public ITransformation
{
public:
  typedef std::pair<Real, Real> CoefficientPairType;

  AffineTransformation(const CoefficientPairType & a_alpha_x,
                       const CoefficientPairType & a_alpha_y)
    : m_alpha_x(a_alpha_x),
      m_alpha_y(a_alpha_y)
  {
    // do nothing
  }

  Point apply(const Point & a_P);
private:
  CoefficientPairType m_alpha_x;
  CoefficientPairType m_alpha_y;
};

#endif
