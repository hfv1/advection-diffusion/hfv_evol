#include <boost/numeric/ublas/io.hpp>

#include "Cell.h"
#include "Mesh.h"
#include "Node.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

template<>
void Cell::setParameter<Integer>(CellParameter a_parameter, const Integer & a_value)
{
  switch(a_parameter) {
  case P_FACENUMBER:
    m_face_ids.push_back(a_value);
    m_number_of_faces = m_face_ids.size();
    break;
  case P_NODENUMBER:
    m_node_ids.push_back(a_value);
    m_number_of_nodes = m_node_ids.size();
    break;
  default:
    std::cerr << "Unknown parameter" << std::endl;
    exit(1);
  };
}

/*----------------------------------------------------------------------------*/

template<>
void Cell::setParameter<Real>(CellParameter a_parameter, const Real & a_value)
{
  switch(a_parameter) {
  case P_MEASURE:
    m_measure = a_value;
    break;
  default:
    std::cerr << "Unknown parameter" << std::endl;
    exit(1);
  };
}

/*----------------------------------------------------------------------------*/

template<>
void Cell::setParameter<Point>(CellParameter a_parameter, const Point & a_value)
{
  switch(a_parameter) {
  case P_CENTER:
    m_center = a_value;
    break;
  default:
    std::cerr << "Unknown parameter" << std::endl;
    exit(1);
  };
}

/*----------------------------------------------------------------------------*/

template<>
const Real & Cell::getParameter<Real>(CellParameter a_parameter)
{
  switch(a_parameter) {
  case P_MEASURE:
    return m_measure;
  default:
    std::cerr << "Unknown parameter" << std::endl;
    exit(1);
  };
}

/*----------------------------------------------------------------------------*/

template<>
const Point & Cell::getParameter<Point>(CellParameter a_parameter)
{
  switch(a_parameter) {
  case P_CENTER:
    return m_center;
  default:
    std::cerr << "Unknown parameter" << std::endl;
    exit(1);
  };
}

/*----------------------------------------------------------------------------*/

Integer Cell::faceId(Integer i) const
{
  return (m_face_ids[i]);
}

/*----------------------------------------------------------------------------*/

Integer Cell::faceLocalId(Integer i) const
{
  Integer j=0;
  for(IdVectorType::const_iterator iF=m_face_ids.begin(); iF!=m_face_ids.end(); iF++, j++)
    if(*iF==i) return j;
  return m_face_ids.size();
}

/*----------------------------------------------------------------------------*/

Integer Cell::nodeId(Integer i) const
{
  return (m_node_ids[i]);
}

/*----------------------------------------------------------------------------*/

Integer Cell::numberOfFaces() const
{
  return m_number_of_faces;
}

/*----------------------------------------------------------------------------*/

Integer Cell::numberOfNodes() const
{
  return m_node_ids.size();
}

/*----------------------------------------------------------------------------*/

const Point &  Cell::center()  const 
{
  return m_center;
}

/*----------------------------------------------------------------------------*/

const Real & Cell::measure() const
{
  return m_measure;
}

/*----------------------------------------------------------------------------*/

void Cell::buildSubMesh(const Mesh * a_Th)
{
  // build subfaces
  m_subfaces.reserve(m_number_of_nodes); 
  for(Integer iP=0; iP<this->numberOfNodes(); iP++) {
    const Node & P = a_Th->node(this->nodeId(iP));
    
    // compute \f$\calF_{T,P}\f$
    std::vector<Integer> calF_TP;
    for(Integer iF=0; iF<this->numberOfFaces(); iF++) {
      const Face & F = a_Th->face(this->faceId(iF));
      for(Integer iQ=0; iQ<F.numberOfPoints(); iQ++) {
        if(F.pointId(iQ)==this->nodeId(iP)) {
          calF_TP.push_back(iF);
          break;
        }
      }
    }
    VF_ASSERT(calF_TP.size()==2);

    // build subface
    // for a subface, the back and front cells are indeed
    // the local face indices within the cell the subface belongs to
    Face S(2, calF_TP[0], calF_TP[1]);
    S.addPoint(-1, P.getPoint());
    S.addPoint(-1, this->center());
    S.compute(Face::P_NORMAL | Face::P_BARYCENTER | Face::P_MEASURE);

    m_subfaces.push_back(S);
  }

  // build subcells
  m_subcells.reserve(m_number_of_faces);
  for(Integer iF=0; iF<this->numberOfFaces(); iF++) {
    const Face & F = a_Th->face(this->faceId(iF));
    Face area(3, -1, -1);
    area.addPoint(-1, F.point(0).first);
    area.addPoint(-1, F.point(1).first);
    area.addPoint(-1, this->center());
    area.compute(Face::P_MEASURE | Face::P_CENTEROFGRAVITY);

    Cell DKF(3, area.centerOfGravity());
    DKF.setParameter(Cell::P_MEASURE, area.measure());

    m_subcells.push_back(DKF);
  }
}

/*----------------------------------------------------------------------------*/

std::ostream & operator<<(std::ostream & a_ostr, const Cell & a_K)
{
  a_ostr << "-- Cell --" << std::endl;
  a_ostr << a_K.m_number_of_faces << " faces" << std::endl;
  a_ostr << "faces  : " << std::flush;
  for(Cell::IdVectorType::const_iterator iF=a_K.m_face_ids.begin(); iF!=a_K.m_face_ids.end(); iF++)
    a_ostr << *iF << " " << std::flush;
  a_ostr << std::endl;
  a_ostr << a_K.m_number_of_nodes << " nodes" << std::endl;
  a_ostr << "nodes   : " << std::flush;
  for(Cell::IdVectorType::const_iterator iP=a_K.m_node_ids.begin(); iP!=a_K.m_node_ids.end(); iP++)
    a_ostr << *iP << " " << std::flush;
  a_ostr << std::endl;
  a_ostr << "center  : " << a_K.center() << std::endl;
  a_ostr << "measure : " << a_K.measure() << std::flush;

  return a_ostr;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
