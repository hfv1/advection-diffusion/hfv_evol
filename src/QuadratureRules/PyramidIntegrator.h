// -*- C++ -*-
#ifndef PYRAMIDINTEGRATOR_H
#define PYRAMIDINTEGRATOR_H

#include "Mesh/Mesh.h"

class PyramidIntegrator 
{
 public:
  PyramidIntegrator(const Mesh * Th, const Integer & iK, const Integer & iF_loc, const Integer & doe = 2);

  inline const std::vector<Point> & points() const { return m_points; }
  inline const Point & point(const Integer & i) const { return m_points[i]; }
  inline const std::vector<Real> & weights() const { return m_weights; }
  inline const Real & weight(const Integer & i) const { return m_weights[i]; }

  inline Integer numberOfPoints() const { return m_points.size(); }

 private:
  const Mesh * m_Th;
  std::vector<Point> m_points;
  std::vector<Real> m_weights;
};

#endif
