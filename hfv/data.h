// -*- C++ -*-
#ifndef data_H
#define data_H

#include <boost/math/constants/constants.hpp>

#include "hfv.h"

using boost::math::constants::pi;

namespace ho
{
  namespace pho
  {
    struct ExactSolution
    {

      typedef std::function<TensorType(const Point &)> DiffusivityType;
      typedef std::function<Real(const Point &)> PotentialType;
      typedef std::function<Real(const Point &)> LoadType;

      typedef std::function<Real(const Real t, const Point &)> ExactSolutionType;
      typedef std::function<Real(const Point &)> InitialDataType;
      typedef std::function<Real(const Point &)> BoundaryType;

      typedef std::function<Eigen::Matrix<Real, DIM, 1>(const Point &)> ExactGradientType;
      typedef std::function<Eigen::Matrix<Real, DIM, 1>(const Point &)> FieldType;


      std::string description_tex;
      Real Advection_param;
      DiffusivityType Lambda;
      PotentialType Phi;
      FieldType V;
      LoadType f;

      ExactSolutionType u;
      InitialDataType u_0;


      BoundaryType u_b;
      ExactGradientType G;


    };

    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------

    struct Regular : public ExactSolution
    {
/*

    Regular(const Real _lx = 0.1, const Real _ly = 1., const Real _v = 1., const Real _r = 0.3  )
      {

      description_tex = "anisotropic exact solution : $ u(t;x;y) = e ^{-\\alpha t + \\frac{y}{2}} \\left ( \\pi \\cos(\\pi y) + \\frac{1}{2} \\sin(\\pi y) \\right ) $ with $\\alpha = \\pi^2 + \\frac{1}{4}$" ;

      Advection_param = -1. ;

      Lambda = [_lx, _ly](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lx , 0. ,
                0. , _ly;
          return   L;
        };


	Phi =[_v](const Point & x) -> Real {
	return -_v *(x(0)*x(0) + x(1) * x(1));
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
             -_v *x(0),
             -_v * x(1);
          return  - this->Lambda(x) * res;
        };



    u = [](const Real t,const Point & x) -> Real {

            return 0. ;
        };


        u_0 =[_r](const Point & x) -> Real {
        if(std::pow(x(0)-0.5,2.)*std::pow(x(1)-0.5,2.)> _r*_r ) {return 1.;}
        else{return 0.;}
	};

	u_b =[this](const Point & x) -> Real {
	return this->u_0(x);
	};

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }

    };
    */

/*
    Regular(const Real & _k = 1. , const Real _lx = 0.01, const Real _ly = 1., const Real _C = 0.1 , const Real _v = 1.  )
      {
        //longtime ??
      description_tex = "anisotropic exact solution : $ u(t;x;y) = e ^{-\\alpha t + \\frac{y}{2}} \\left ( \\pi \\cos(\\pi y) + \\frac{1}{2} \\sin(\\pi y) \\right ) $ with $\\alpha = \\pi^2 + \\frac{1}{4}$" ;

      Advection_param = -1. ;

      Lambda = [_lx, _ly](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lx , 0. ,
                0. , _ly;
          return   L;
        };


	Phi =[_v](const Point & x) -> Real {
	return -_v *x(0);
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
             -_v,
             0.;
          return  - this->Lambda(x) * res;
        };



    u = [_k, _v, _lx,_C](const Real t,const Point & x) -> Real {
            Real alpha = _lx*(pi<Real>()*pi<Real>()*_k*_k  + 0.25*_v*_v);

            Real exp_fact_x = exp(- alpha * t  + 0.5*_v*x(0));

            Real esp_x =  2. * _k * pi<Real>() * (1. / _v)* cos(_k * pi<Real>() * x(0)) + sin( _k*pi<Real>() * x(0));

            Real infty_esp = exp(_v*x(0));
            Real infty_mass = 2.*_C * _k * pi<Real>() * exp(-0.5*_v );

            Real infty  = infty_mass * infty_esp;



            return infty + _C* exp_fact_x * esp_x ;
        };


    u_0 =[this](const Point & x) -> Real {
        return this->u(0.,x);
//        Real f;
//        if(x(1) * (x(1)-1.) * x(0)*(x(0)-1.) < 0.05){ f = 0.1;}
//        else{f =0.;}
//        return f;
        };

	u_b =[this](const Point & x) -> Real {
	return this->u(0.,x);
	};

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }

    };
    */

/*
    Regular(const Real & _k = 1. , const Real _lx = 0.8, const Real _ly = 1., const Real _C = 0.1 , const Real _v = 10.  )
      {
        //positivity ??
      description_tex = "anisotropic exact solution : $ u(t;x;y) = e ^{-\\alpha t + \\frac{y}{2}} \\left ( \\pi \\cos(\\pi y) + \\frac{1}{2} \\sin(\\pi y) \\right ) $ with $\\alpha = \\pi^2 + \\frac{1}{4}$" ;

      Advection_param = -1. ;

      Lambda = [_lx, _ly](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lx , 0. ,
                0. , _ly;
          return   L;
        };


	Phi =[_v](const Point & x) -> Real {
	return -x(1);//0.*0.001 * ( (x(0)-0.5) * (x(0)-0.5) + (x(1)-0.5) * (x(1)-0.5));
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
                0.,//0.*0.002*(x(0)-0.5),
                1.;//0.*0.002*(x(1)-0.5);
          return  - this->Lambda(x) * res;
        };



    u = [_k, _v, _lx,_C](const Real t,const Point & x) -> Real {
            Real alpha = _lx*(pi<Real>()*pi<Real>()*_k*_k  + 0.25*_v*_v);

            Real exp_fact_x = exp(- alpha * t  + 0.5*_v*x(0));

            Real esp_x =  2. * _k * pi<Real>() * (1. / _v)* cos(_k * pi<Real>() * x(0)) + sin( _k*pi<Real>() * x(0));

            Real infty_esp = exp(_v*x(0));
            Real infty_mass = 2.*_C * _k * pi<Real>() * exp(-0.5*_v );

            Real infty  = infty_mass * infty_esp;



            return infty + _C* exp_fact_x * esp_x ;
        };


    u_0 =[this](const Point & x) -> Real {
       // return this->u(0.,x);
        Real f;
        if((x(1)-0.5)*(x(1)-0.5) + (x(0)-0.5)*(x(0)-0.5) <0.2*0.2){ f = 0.001;}
        else{f =1.;}
        return f;
        };

	u_b =[this](const Point & x) -> Real {
	return this->u(0.,x);
	};

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }

    };
*/


 Regular(const Real & _k = 1. , const Real _lx = 0.8, const Real _ly = 1., const Real _C = 0.1 , const Real _v = 10.  )
      {
        //positivity ??
      description_tex = "positivity test" ;

      Advection_param = -1. ;

      Lambda = [_lx, _ly](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lx , 0. ,
                0. , _ly;
          return   L;
        };


	Phi =[_v](const Point & x) -> Real {
	return -1 * ( (x(0)-0.4) * (x(0)-0.4) + (x(1)-0.6) * (x(1)-0.6));
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
                -2*(x(0)-0.4),
                -2*(x(1)-0.6);
          return  - this->Lambda(x) * res;
        };



    u = [_k, _v, _lx,_C](const Real t,const Point & x) -> Real {
            Real alpha = _lx*(pi<Real>()*pi<Real>()*_k*_k  + 0.25*_v*_v);

            Real exp_fact_x = exp(- alpha * t  + 0.5*_v*x(0));

            Real esp_x =  2. * _k * pi<Real>() * (1. / _v)* cos(_k * pi<Real>() * x(0)) + sin( _k*pi<Real>() * x(0));

            Real infty_esp = exp(_v*x(0));
            Real infty_mass = 2.*_C * _k * pi<Real>() * exp(-0.5*_v );

            Real infty  = infty_mass * infty_esp;



            return infty + _C* exp_fact_x * esp_x ;
        };


    u_0 =[this](const Point & x) -> Real {
       // return this->u(0.,x);
        Real f;
        if((x(1)-0.5)*(x(1)-0.5) + (x(0)-0.5)*(x(0)-0.5) <0.2*0.2){ f = 0.001;}
        else{f =1.;}
        return f;
        };

	u_b =[this](const Point & x) -> Real {
	return this->u(0.,x);
	};

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }

    };

/*
    Regular(const Real & _k = 1. , const Real _lx = 1.,  const Real _L = 1., const Real _v = -1.  )
      {

      description_tex = "anisotropic exact solution : $ u(t;x;y) = e ^{-\\alpha t + \\frac{y}{2}} \\left ( \\pi \\cos(\\pi y) + \\frac{1}{2} \\sin(\\pi y) \\right ) $ with $\\alpha = \\pi^2 + \\frac{1}{4}$" ;

      Advection_param = -1. ;

      Real V_r = (2. / (_L * _v) )* (0.25*_v*_v + _k*_k *pi<Real>()*pi<Real>() *(1. - _L) );
      Real v_y = std::sqrt(V_r) * _v;

      Lambda = [_lx, _L](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lx , 0. ,
                0. , _lx / _L ;
          return   L;
        };


	Phi =[_v,v_y](const Point & x) -> Real {
	return _v *x(0) + v_y * x(1);
	};

	V = [this,_v,v_y](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
             _v,
             v_y ;
          return  - this->Lambda(x) * res;
        };



    u = [_k, _v, _lx,v_y](const Real t,const Point & x) -> Real {
            Real alpha = _lx*(pi<Real>()*pi<Real>()*_k*_k  + 0.25*_v*_v);
            Real exp_fact_x = exp(- alpha * t  - 0.5*_v*x(0));
            Real exp_fact_y = exp(- alpha * t  - 0.5* v_y*x(1));
            Real esp_x =  -2. * _k * pi<Real>() * (1. / _v)* cos(_k * pi<Real>() * x(0)) + sin( _k*pi<Real>() * x(0));
            Real esp_y =  -2. * _k * pi<Real>() * (1. / v_y ) * cos(_k * pi<Real>() * x(1)) + sin( _k*pi<Real>() * x(1));
            Real infty_esp = exp(-_v*x(0) - v_y*x(1));
            Real infty_mass = exp(_v + v_y) * ( exp(-0.5*_v)*(2.*_k*pi<Real>()/_v) + exp(-0.5*v_y)*(2.*_k*pi<Real>()/v_y ));

            Real infty  = infty_mass * infty_esp;

            Real spa = exp_fact_x * esp_x + exp_fact_y * esp_y;



            return infty +  spa ;
        };


        u_0 =[this](const Point & x) -> Real {
	return this->u(0.,x);
	};

	u_b =[this](const Point & x) -> Real {
	return this->u(0.,x);
	};

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }

    };*/

    /*
    Regular(const Real & _nu = 0.0001  )
      {

      description_tex = "anisotropic exact solution : $ u(t;x;y) = e ^{-\\alpha t + \\frac{y}{2}} \\left ( \\pi \\cos(\\pi y) + \\frac{1}{2} \\sin(\\pi y) \\right ) $ with $\\alpha = \\pi^2 + \\frac{1}{4}$" ;

      Advection_param = -1. ;

      Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _nu , 0. ,
                0. , 10. ;
          return   L;
        };


	Phi =[](const Point & x) -> Real {
	return -x(0);
	};

	V = [this](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
             -1.,
             0. ;
          return  - this->Lambda(x) * res;
        };



    u = [_nu](const Real t,const Point & x) -> Real {
            Real alpha = _nu*(pi<Real>()*pi<Real>() + 0.25);
            Real exp_fact = exp(- alpha * t  + 0.5*x(0));
            Real exp_sum = pi<Real>() * exp((x(0) - 0.5));

            return 1.*(exp_fact* (pi<Real>() * cos(pi<Real>() * x(0)) + 0.5* sin( pi<Real>() * x(0))  ) + exp_sum) ;
        };


        u_0 =[this](const Point & x) -> Real {
	return this->u(0.,x);
	};

	u_b =[this](const Point & x) -> Real {
	return this->u(0.,x);
	};

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }

    };
*/
  /* Regular(const Real & _nu = 0.001  )
      {

      description_tex = "anisotropic exact solution : $ u(t;x;y) = e ^{-\\alpha t + \\frac{y}{2}} \\left ( \\pi \\cos(\\pi y) + \\frac{1}{2} \\sin(\\pi y) \\right ) $ with $\\alpha = \\pi^2 + \\frac{1}{4}$" ;

      Advection_param = -1. ;

      Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _nu , 0. ,
                0. , 1. ;
          return   L;
        };


	Phi =[](const Point & x) -> Real {
	return -x(0);
	};

	V = [this](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
             -1.,
             0. ;
          return  - this->Lambda(x) * res;
        };



    u = [_nu](const Real t,const Point & x) -> Real {
            Real alpha = _nu*(pi<Real>()*pi<Real>() + 0.25);
            Real exp_fact = exp(- alpha * t  + 0.5*x(0));
            Real exp_sum = pi<Real>() * exp((x(0) - 0.5));

            return exp_fact* (pi<Real>() * cos(pi<Real>() * x(0)) + 0.5* sin( pi<Real>() * x(0))  ) + exp_sum ;
        };


        u_0 =[this](const Point & x) -> Real {
	return this->u(0.,x);
	};

	u_b =[this](const Point & x) -> Real {
	return this->u(0.,x);
	};

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }

    };
*/

    /*Regular(const Real & _nu = 1., const Real _v = 10., const Real _M = 1. )
      {

       description_tex = " exact solution : $ u(t;x;y) = e ^{-\\alpha t - \\frac{v}{2} y } \\left ( \\cos(\\pi y) - \\frac{v}{2 \\pi} \\sin(\\pi y) \\right ) + e^{\\frac{v}{2} - v y} $ with $\\alpha = - \\frac{4 \\pi^2 + v^2 }{4}$ and $v =" +to_string(_v) +"$" ;

      Advection_param = _v ;

        Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1. ;
          return  _nu * L;
        };

        Phi =[_v](const Point & x) -> Real {
	return  _v * x(1);
	};

        V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
            Eigen::Matrix<Real, 2, 1> res;
            res <<
                0.,
                _v ;
          return  - this->Lambda(x) * res;
        };


        u = [_nu, _v, _M](const Real t,const Point & x) -> Real {
            Real alpha =  _nu * 0.25* ( 4.* pi<Real>()*pi<Real>()  +  _v * _v );
            Real exp_fact = exp(- alpha * t  - 0.5* _v * x(1));
            Real exp_sum =exp(0.5 * _v - _v * x(1));

            Real res = exp_fact* ( cos(pi<Real>() * x(1)) - 0.5* (_v / pi<Real>()) * sin( pi<Real>() * x(1))  ) + exp_sum ;
            return _M * res;
          //return exp(x(1));
        };

	u_0 = [this](const Point & x) -> Real {  return this->u(0.,x);
	};



        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };

          }
}; */







  } // namespace pho
} // namespace ho

#endif
