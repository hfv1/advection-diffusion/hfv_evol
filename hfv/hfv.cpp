#include "hfv.h"


#include <Common/GetPot>

#include <boost/integer/static_min_max.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/numeric/ublas/vector.hpp>

#include "Common/chrono.hpp"

#include "data.h"
#include "postProcessing.h"

#include "Eigen/SparseCore"
#include "Eigen/SparseLU"

#include <unsupported/Eigen/SparseExtra>

#include <vector>

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <string.h>

#ifndef Eta
#define Eta 1.5
#endif

#ifndef s_pos
#define s_pos std::pow(10., -11)
#endif

#ifndef s_iter
#define s_iter 50
#endif

#ifndef s_G
#define s_G 5.*std::pow(10.,-9) //-13)
#endif

#ifndef s_R
#define s_R 5.*std::pow(10., -7)
#endif

#ifndef s_R_rel
#define s_R_rel 1.*std::pow(10., -11)
#endif

#ifndef modtps
#define modtps 50
#endif

#ifndef nb_pts_in_graph
#define nb_pts_in_graph 12
#endif


/*
#ifndef D_t
#define D_t 0.005
#endif
*/



using boost::math::constants::pi;

using namespace ho;

//------------------------------------------------------------------------------

typedef Eigen::Matrix<std::size_t, Eigen::Dynamic, 1> IndexVectorType;
typedef Eigen::SparseMatrix<Real> SparseMatrixType;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> SolutionVectorType;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> ComputeFluxType;
typedef Eigen::Triplet<Real> TripletType;
typedef Eigen::Matrix<Real, 2, 2 > TensorType;
typedef std::function<TensorType(const Point &)> DiffusivityType;
typedef std::function<Eigen::Matrix<Real, 2, 1>(const Point &)> FieldType;
typedef std::function<Real(const Point &)> FctContType;

//int omegaschema(const Real & epsilon, const  Real & T,const  int & N_iter, const bool Visu ,Integer argc, char * argv[]);
//int non_linear(const  Real & T_end ,const  int & Nb_iter, const bool Visu , Integer argc, char * argv[]);
vector<Real> omegaschema_mesh(const Real & epsilon, const  Real & T,const  int & N_iter, const string & Mesh_file ,const bool Visu ,Integer argc, char * argv[]);
vector<Real> omegaschema_mesh_harmo(const Real & epsilon, const int type_moy, const  Real & T,const  int & N_iter, const string & Mesh_file ,const bool Visu ,Integer argc, char * argv[]);
//type_moy = 1 : harmonique par pyramide
vector<Real> non_linear_mesh(const  Real & T_end ,const  int & Nb_iter,const string & Mesh_file , const bool Visu , Integer argc, char * argv[]);

//------------------------------------------------------------------------------
int main(Integer argc, char * argv[]){
    bool Visu = false;

    //const string & geo_mesh = "hexagonal";

    //const int & i_max = 5;
    string  geo_mesh ; // = "hexagonal";
    int  i_max ; //= 5;

    Real T; //= 0.1;
    int N_iter_ini; // = 5;
    /*
    cout << "Mesh ? (name with number, ex : hexagonal_3 or mesh3_5)" << endl;
    cin >> geo_mesh ;

    cout << "Final time ? " << endl;
    cin >> T;

    cout << "Number of iteration ? " << endl;
    cin >> N_iter_ini;        
    */

    geo_mesh = "pi6_tiltedhexagonal_5"; 
    T =  5.*std::pow(10.,-4);
    N_iter_ini = 49; 




    ho::pho::Regular sol;
    //Real v = sol.Advection_param;
    string desc = sol.description_tex;

    const int nb_schema_lin = 2;
    Real List_epsilon[nb_schema_lin];
    List_epsilon[0] = 0.;
    List_epsilon[1] = 1.;

    const int nb_schema = nb_schema_lin +1;

    //Real T = 0.1;
    //int N_iter_ini = 5;

    std::string Mesh_base = "../meshes/";
    std::string Mesh_file = Mesh_base + geo_mesh +".typ1";

    vector<Real> Data_errors_non_lin = non_linear_mesh( T, N_iter_ini, Mesh_file, Visu ,argc, argv);
    vector<Real> Data_errors_Droniou = omegaschema_mesh(0., T, N_iter_ini, Mesh_file, Visu ,argc, argv);
    vector<Real> Data_errors_expfitt_ari = omegaschema_mesh(1., T, N_iter_ini, Mesh_file, Visu ,argc, argv);
    vector<Real> Data_errors_expfitt_harmo = omegaschema_mesh_harmo(1., 1 , T, N_iter_ini, Mesh_file, Visu ,argc, argv);
//
//    vector<vector<vector<Real>>> Data_errors;
//    Data_errors.resize(nb_schema);
//    for(int i =0 ; i < nb_schema ; i ++){
//        Data_errors[i].resize(i_max);
//    }
//
//    //omegaschema(0., T, N_iter, Visu ,argc, argv);
//    //omegaschema(1./v ,argc, argv);
//    //omegaschema(0.5, T, N_iter, Visu  ,argc, argv);
//    //omegaschema(1., T, N_iter, Visu ,argc, argv);
//    for(int i=1; i< i_max+1 ; i++){
//        std::string Mesh_file = Mesh_base + geo_mesh +"_" + std::to_string(i) +".typ1";
//        int N_iter = N_iter_ini * std::pow(4,i-1);
//
//        Data_errors[0][i-1] = non_linear_mesh( T, N_iter, Mesh_file, Visu ,argc, argv); //ici, mettre schema non linéaire
//
//        for(int sch_lin =0 ; sch_lin < nb_schema_lin; sch_lin ++){
//            Data_errors[sch_lin+1][i-1] = omegaschema_mesh(List_epsilon[sch_lin], T, N_iter, Mesh_file, Visu ,argc, argv);
//        }//for sch_lin
//    } //for i
//
//    //create file for errors
//
//    std::ofstream myfile;
//    myfile.open ("errors.csv");
//    myfile << "Première cellule premiere colonne .\n";
//    myfile <<"Description "<<"," <<"Convection diffusion, omega schéma .\n";
//    myfile <<"Eta"<<"," << Eta  << endl;
//    myfile <<"Maillage"<<"," << geo_mesh  << endl;
//    myfile <<"" << endl;
//    for(int sch = 0; sch< nb_schema; sch ++){
//        myfile << "Meshsize, L ^\infty (L^2 )error (rel),L ^2 (H^1 ) error (rel), Minimum ,\n";
//        for(int mesh_int = 0; mesh_int < i_max; mesh_int ++){
//            myfile << Data_errors[sch][mesh_int][0] <<","<< Data_errors[sch][mesh_int][1] <<","<< Data_errors[sch][mesh_int][2] <<","<< Data_errors[sch][mesh_int][3] << endl;
//            }//mmesh_int
//    }//for sch
//      myfile.close();
//
//      //create file graph
//
//    std::ofstream file_graph;
//    file_graph.open ("errors_graph.txt");
//    file_graph << nb_schema << "," << i_max<< endl;
//    file_graph << geo_mesh << "," << T << "," <<N_iter_ini << endl;
//    file_graph << desc << "," << v << endl;
//    file_graph << Eta  << endl;
//    file_graph << "non-linear" << "," ;
//    for(int sch_lin =0 ; sch_lin < nb_schema_lin; sch_lin ++){
//        file_graph << "$\\epsilon = " << to_string(List_epsilon[sch_lin]) << "$,";
//    } //for sch_lin
//    file_graph << endl ;
//    for(int sch = 0; sch< nb_schema; sch ++){
//        for(int mesh_int = 0; mesh_int < i_max; mesh_int ++){
//            file_graph << Data_errors[sch][mesh_int][0] <<","<< Data_errors[sch][mesh_int][1] <<","<< Data_errors[sch][mesh_int][2] <<","<< Data_errors[sch][mesh_int][3] << endl;
//            }//mmesh_int
//    }//for sch
//      file_graph.close();

    std::cout << "Done" << std::endl;
    return 0;
}

//linear (omega)

vector<Real> omegaschema_mesh(const Real & epsilon, const  Real & T,const  int & N_iter, const string & Mesh_file ,const bool Visu ,Integer argc, char * argv[])
{
  std::string sep = "\n----------------------------------------\n";
  Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
  std::cout << "test1 ? "  << std::endl;

  // Read options and data
  GetPot options(argc, argv);

  std::string mesh_file = Mesh_file;
//  std::string mesh_file = "../meshes/mesh1_1.typ1";
//  if(options.search(2, "--mesh", "-m")) { mesh_file = options.next(""); }
//    std::cout << mesh_file  << std::endl;
  // Pretty-print floating point numbers
  std::cout.precision(2);
  std::cout.flags(std::ios_base::scientific);

  std::cout << "test2 ? "  << std::endl;
  // Create mesh
  std::shared_ptr<Mesh> Th(new Mesh());
  if(mesh_file.find("typ1")!=std::string::npos)
    Th->readFVCA5File(mesh_file.c_str());
  else if(mesh_file.find("dgm")!=std::string::npos)
    Th->readDGMFile(mesh_file.c_str());
  else {
    std::cerr << "Unknown mesh format" << std::endl;
    exit(1);
  }

  std::cout << "test3 ? "  << std::endl;

  Th->buildFaceGroups();
  std::cout << "MESH: " << mesh_file << std::endl;
  std::cout << FORMAT(50) << "num_cells" << Th->numberOfCells() << std::endl;
  std::cout << FORMAT(50) << "num_faces" << Th->numberOfFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws_cells" << Th->numberOfCells()  << std::endl;
  std::cout << FORMAT(50) << "num_unknws_faces" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws" << Th->numberOfCells() +  Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws_with_elimination" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;

  //------------------------------------------------------------------------------
  // Estimate mesh size
  std::cout << FORMAT(50) << "Temps final "<< T<< std::endl;
  std::cout<< FORMAT(50) << "Paramètre de stabilisation" << Eta << std::endl;

  Real h = Th->meshsize();
  std::cout << FORMAT(50) << "meshsize" << h << std::endl;
  Real D_t = T / (N_iter+1.);
  std::cout << FORMAT(50) << "time step size" << D_t << std::endl;



  //------------------------------------------------------------------------------
  // Exact solution

  ho::pho::Regular sol;

  // omega function and modified tensor and field

  //Real epsilon = 0.001;

  const FctContType omega = [epsilon,sol](const Point & x) -> Real {
    Real arg = - epsilon * sol.Phi(x);
    return std::exp(arg);
  };

  const DiffusivityType Lambda_omega = [sol, omega](const Point & x) -> TensorType {
    return omega(x) * sol.Lambda(x);
  };

  const FieldType V_omega = [sol, omega, epsilon](const Point & x) -> Eigen::Matrix<Real, 2, 1>  {
    return (1. - epsilon) * omega(x) * sol.V(x);
  };



  //------------------------------------------------------------------------------
  // Assemble matrix
  //------------------------------------------------------------------------------


  int nunkw = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
  int n_vol_unkw =  Th->numberOfCells();

  // Pre computation
  std::cout << "Pre-computation begins" << endl;
  common::chrono c_precomput;
  c_precomput.start();

  std::vector<vector<vector<Real>>> A;
  comp_A_elt(Th.get(), Lambda_omega, Eta, A);

  std::vector<vector<Real>> B;
  comp_B_elt(Th.get(), A, B );

  std::vector<Real> Alpha;
  comp_Alpha_elt(Th.get(), B, Alpha );

  std::vector<Real>  Diff_Int;
  comp_DiffInt(Th.get(),  Lambda_omega, Diff_Int );


  std::vector<vector<Real>> Conv_N;
  std::vector<vector<Real>> Conv_P;
  comp_Conv_elt(Th.get(), Diff_Int, V_omega,  Conv_N, Conv_P);



  std::vector<Real>  Conv_N_sum;
  std::vector<Real>  Conv_P_sum;
  comp_Alpha_elt(Th.get(), Conv_N , Conv_N_sum);
  comp_Alpha_elt(Th.get(), Conv_P , Conv_P_sum);

  //discrétisation donnée au bord (relevement) et donnée initiale
  Real Masse = 0.;
  SolutionVectorType U_0 = SolutionVectorType::Zero(n_vol_unkw);
  SolutionVectorType X_0 = SolutionVectorType::Zero(nunkw);
  for(int iT = 0; iT< Th->numberOfCells(); iT ++){
    const Cell & T = Th->cell(iT);
    const Real & m_T = T.measure();
    const Point & x_T = T.center();
    U_0(iT) = sol.u_0(x_T);
    Masse += m_T * U_0(iT);
  }//for iT

  std::cout << FORMAT(50) << "Discrete mass"  << Masse << std::endl;

    for(int iF = 0; iF < nunkw ; iF ++){
        const Face & F = Th->face(iF);
        const Point & x_F = F.barycenter();
        X_0(iF) = sol.u_0(x_F);
    }// for iF_loc


  c_precomput.stop();
  cout << "Pre-computation ends" << endl;
  std::cout << FORMAT(50) << "time_precomputation"  << c_precomput.diff() << std::endl;


    std::vector<SolutionVectorType> Sol_faces;
    std::vector<SolutionVectorType> Sol_maille;
    std::vector<Real> Min_on_mesh;
    std::vector<Real> Min_on_edge;
    std::vector<Real> Temps;

    int Nb_neg_unk = 0;
    int Nb_neg_cells_unk =0; 
    int Nb_neg_faces_unk =0; 
    int Nb_unk = 0;

    Sol_maille.push_back(U_0);
    Sol_faces.push_back(X_0);
    Temps.push_back(0.);

    common::chrono c_assembly;
    c_assembly.start();
    cout << "matrix assembly begins" << endl;



    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_edge_max =Th->maximumNumberOfFaces();

    //compute h -> u matrixes
    std::vector<TripletType> Triplets_Mat_mass;
    Triplets_Mat_mass.reserve(n_vol);
    std::vector<TripletType> Triplets_D_vol;
    Triplets_D_vol.reserve(n_vol);
    std::vector<TripletType> Triplets_D_edge;
    Triplets_D_edge.reserve(n_edge);

    for(int iT = 0; iT < n_vol ; iT ++){
        const Cell & T = Th->cell(iT);
        const Point & x_T = T.center();
        const Real & omega_T = omega(x_T);
        const Real & m_T = T.measure();
        Triplets_D_vol.push_back(TripletType(iT, iT, 1./omega_T));
        Triplets_Mat_mass.push_back(TripletType(iT, iT, m_T));
    } //for iT

    SparseMatrixType Mat_mass(n_vol, n_vol);
    Mat_mass.setFromTriplets(Triplets_Mat_mass.begin(), Triplets_Mat_mass.end());
    Mat_mass = (1./D_t)*Mat_mass;
    SparseMatrixType D_vol(n_vol, n_vol);
    D_vol.setFromTriplets(Triplets_D_vol.begin(), Triplets_D_vol.end());

    for(int iF = 0; iF < n_edge ; iF ++){
        const Face & F = Th->face(iF);
        const Point & x_F = F.barycenter();
        const Real omega_F = omega(x_F);
        Triplets_D_edge.push_back(TripletType(iF, iF, 1./omega_F));
    } //for iF

    SparseMatrixType D_edge(n_edge, n_edge);
    D_edge.setFromTriplets(Triplets_D_edge.begin(), Triplets_D_edge.end());



    // compute triplets ( h linear system)
    std::vector<TripletType> Triplets_invM1;
    Triplets_invM1.reserve(n_vol);
    std::vector<TripletType> Triplets_M2;
    Triplets_M2.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_M3;
    Triplets_M3.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_M4;
    Triplets_M4.reserve(n_vol* n_edge_max * n_edge_max);

    triplets_invM1andM2_evol(Th.get(), D_t, omega,  Alpha, B, Conv_N_sum,  Conv_P,
    Triplets_invM1,  Triplets_M2);

    triplets_M3andM4(Th.get(),  B, A, Conv_N, Conv_P,
    Triplets_M3, Triplets_M4);

    //compute matrix
    SparseMatrixType invM1(n_vol, n_vol);
    invM1.setFromTriplets(Triplets_invM1.begin(), Triplets_invM1.end());
    SparseMatrixType M2(n_vol, n_edge);
    M2.setFromTriplets(Triplets_M2.begin(), Triplets_M2.end());
    M2 = M2 * D_edge;
    SparseMatrixType M3(n_edge, n_vol);
    M3.setFromTriplets(Triplets_M3.begin(), Triplets_M3.end());
    M3 = M3 * D_vol;
    SparseMatrixType M4(n_edge, n_edge);
    M4.setFromTriplets(Triplets_M4.begin(), Triplets_M4.end());
    M4 =M4 *D_edge;

    SolutionVectorType  Sec_m_vol = SolutionVectorType::Zero(n_vol);

    comp_sec_memb_Neumann(Th.get()  , sol.f, Sec_m_vol );



    SparseMatrixType GLOBAL_LHS =  (M4 -M3*invM1 * M2) ;
    Eigen::VectorXd GLOBAL_RHS =  ( - M3*invM1 * Sec_m_vol);

    c_assembly.stop();
    cout << "Matrix assembly ends" << endl;
    std::cout << FORMAT(50) << "time_assembly"  << c_assembly.diff() << std::endl;

    // Solve linear system and reconstruct mesh unknowns

    common::chrono c_facto;
    c_facto.start();

    cout << "Linear solving begins" << endl;

    SolutionVectorType X = SolutionVectorType::Zero(nunkw);
    SolutionVectorType U = SolutionVectorType::Zero(n_vol_unkw);

    Eigen::SparseLU<SparseMatrixType> solver_evo;
    // Compute the ordering permutation vector from the structural pattern of ...
    solver_evo.analyzePattern(GLOBAL_LHS);
    std::cout << FORMAT(50) << "analyse pattern OK"  << std::endl;
    // Compute the numerical factorization
    solver_evo.factorize(GLOBAL_LHS);
    c_facto.stop();
    std::cout << FORMAT(50) << "factorisation etat stationnaire  OK"  << std::endl;
    std::cout << FORMAT(50) << "time_facto"  << c_facto.diff() << std::endl;


    //Use the factors to solve the linear system
    int Nb_resol_lin = 0;
    common::chrono c_solve;
    c_solve.start();
    for(int n = 0 ; n<  N_iter +1; n ++){

        if(n%200==0){std::cout << "Calcul de la "<< n+1 << "-eme  itération" << std::endl; }
        SolutionVectorType X = SolutionVectorType::Zero(nunkw);
        SolutionVectorType U = SolutionVectorType::Zero(n_vol_unkw);

        Eigen::VectorXd GLOBAL_RHS_n =  - M3*invM1 * ( Sec_m_vol + Mat_mass * Sol_maille[n] );

        X = (solver_evo.solve(GLOBAL_RHS_n));
        Nb_resol_lin += 1;
        U = invM1 * (Sec_m_vol + Mat_mass * Sol_maille[n] - M2*X);
        Sol_faces.push_back(X);
        Sol_maille.push_back(U);
        Min_on_mesh.push_back(U.minCoeff());
        Min_on_edge.push_back(X.minCoeff());

        for(int iT =0 ; iT < n_vol; iT ++){
            if(U(iT)< 0.){
            Nb_neg_cells_unk += 1; 
            Nb_neg_unk += 1;}
        } //for iT, count the numberof negative mesh unk

        for(int iF =0 ; iF < n_edge; iF ++){
            if(X(iF)< 0.){
            Nb_neg_faces_unk += 1; 
            Nb_neg_unk += 1;}
        } //for iF, count the numberof negative faces unk


        Nb_unk += n_vol + n_edge;

        Temps.push_back((n +1.) * D_t);
    }

    c_solve.stop();
    std::cout << FORMAT(50) << "fin calcul "  << std::endl;
    std::cout << FORMAT(50) << "time_solve"  << c_solve.diff() << std::endl;


    //Creation etat stationnaire discret
    std::cout << "Discretisation of steady state begins" << endl;

    Real rho = 0;
    for(int iT =0 ; iT < n_vol_unkw ; iT ++){
        const Cell & T = Th->cell(iT);
        const Real & m_T = T.measure();
        const Point & x_T = T.center();
        rho += m_T * exp(-sol.Phi(x_T));
    }//for iT

  rho = Masse/rho;

    SolutionVectorType U_infty = SolutionVectorType::Zero(n_vol_unkw);
    SolutionVectorType X_infty = SolutionVectorType::Zero(nunkw);
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Point & x_T = T.center();
        U_infty(iT) = rho*exp(-sol.Phi(x_T));
        }//for iT

      for(int iF = 0; iF< nunkw;iF++){
          const Face & F = Th->face(iF);
          const Point & x_F = F.barycenter();
          X_infty(iF) = rho*exp(-sol.Phi(x_F));
          } //for iF

    std::cout << "Discretisation of steady state ends" << endl;

    std::cout << sep << std::endl;
    std::cout << FORMAT(50) << "Errors"  << std::endl;
    std::cout << sep << std::endl;

    //Entropies et dissipations

    std::cout << sep << "Calcul des entropies et dissipation" << std::endl;


    std::vector<Real> Diff_L2_square;
    std::vector<Real> Diff_L1_square;
    std::vector<Real> Diff_H1;

    for(int n = 0;n<  N_iter+2; n ++)
    {
        Real dif_h1 =0.;
        Real diff_l2 = 0.;
        Real diff_l1 = 0.;

        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
            const Cell & T = Th->cell(iT);
            const Real m_T = T.measure();
            const Point x_T = T.center();
            const int m = T.numberOfFaces();

            diff_l2+= m_T*std::pow( U_infty(iT) -Sol_maille[n](iT) , 2);
            diff_l1+= m_T*std::fabs( U_infty(iT) -Sol_maille[n](iT));


	// Compute local error in energy-like norm (discrete H^1-norm)
            for(int iF_loc=0;iF_loc<m;iF_loc++ ){
                const int iF=T.faceId(iF_loc);
                const Face & F = Th->face(iF);
                const Real m_F = F.measure();
                const Point x_F = F.barycenter();
                const Point n_F= F.normal(x_T);
                const Real d_F = inner_prod(n_F, x_F-x_T);

                dif_h1 += (m_F/d_F)*std::pow(U_infty(iT)-X_infty(iF)-Sol_maille[n](iT) + Sol_faces[n](iF),2);
            }// for iF_loc
        } // for iT

    Diff_L2_square.push_back(diff_l2);
    Diff_L1_square.push_back(diff_l1*diff_l1);
    Diff_H1.push_back(dif_h1);

    } // for n

   std::cout << "Entropies et dissipations calculées"  << std::endl;
   const int mod =round(N_iter / nb_pts_in_graph) +1;


   std::ofstream myfile;
      myfile.open ("temps_long_eps"+ to_string(epsilon) + ".csv");
      myfile << "Première cellule premiere colonne .\n";
      myfile <<"Description "<<"," <<"Convection diffusion, omega schéma arithmetique .\n";
      myfile <<"Epsilon"<<"," << epsilon << endl;
      myfile <<"Eta"<<"," << Eta  << endl;
      myfile <<"Pas de temps"<<"," << D_t  << endl;
      myfile <<"Nombre d'itérations "<<"," << N_iter  << endl;
      myfile <<"Maillage"<<"," << mesh_file  << endl;
      myfile <<"Finesse de maillage"<<"," << h  << endl;
      myfile <<"" << endl;
      myfile << "Temps,Diff_L1_square,Diff_L2_square,Diff_H1_square,\n";
      for(int n=0; n< N_iter+2; n ++){
        myfile << Temps[n] <<","<<Diff_L1_square[n] <<"," << Diff_L2_square[n] << ","<< Diff_H1[n]  << endl;
      }
      myfile.close();

      std::ofstream myfile2;
      if(epsilon ==0.){myfile2.open ("tps_HMM");}
      if(epsilon ==1.){myfile2.open ("tps_EFari");}
      myfile2 << "Temps Diff_L1_square Diff_L2_square Diff_H1_square Diff_L1 Diff_L2 \n";
      for(int n=0; n< N_iter+2; n ++){
        if(n % mod ==0){
            myfile2 << Temps[n] <<" "<<Diff_L1_square[n] <<" " << Diff_L2_square[n] << " "<< Diff_H1[n] << " "<< std::sqrt(Diff_L1_square[n]) <<" " << std::sqrt(Diff_L2_square[n]) << endl;
        }// if n % 100 ==0
      }//for n
      myfile2.close();

    std::cout <<  "Enregistrement fichier Entropies" << std::endl;

    if(Visu){
        std::cout << sep << "Fichiers visualisation" << std::endl;

        std::vector<Eigen::VectorXd> coeffs_u_infty(Th->numberOfCells());
        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
            Eigen::VectorXd uh_F_infty (1);
            uh_F_infty(0) =U_infty(iT);
            coeffs_u_infty[iT] = uh_F_infty;
        }
      postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>("output/uh_infty.vtu", Th.get(), coeffs_u_infty);

      std::cout <<  "Enregistrement fichier visualisation - etat stationnaire " << std::endl;

      for(int i=0; i< N_iter+2; i++ ){
        if(i % mod ==0){
            std::vector<Eigen::VectorXd> coeffs_u(Th->numberOfCells());
            for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
              Eigen::VectorXd uh_F_end (1);
              uh_F_end(0) =Sol_maille[i](iT);
              coeffs_u[iT] = uh_F_end;
            } // for iT

            char str[20] = {0};
            std::sprintf(str, "%d", i);
            char name_c[40];
            strcpy(name_c,"output/uh_");
            strcat(name_c, str);
            strcat(name_c,".vtu");
            postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>(name_c, Th.get(), coeffs_u);
        }


      } //for i


      std::cout <<  "Enregistrement fichier visualisation - evolutif " << std::endl;

      std::vector<Eigen::VectorXd> coeffs_u_end(Th->numberOfCells());
      for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
        Eigen::VectorXd uh_F_end (1);
        uh_F_end(0) =Sol_maille[N_iter+1](iT);
        coeffs_u_end[iT] = uh_F_end;
      }
      postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>("output/uh_end.vtu", Th.get(), coeffs_u_end);
  }
    //Calcul erreur cas test et vérification, norme L^infty_t L²_x

    std::cout << sep << "Calcul erreurs - cas test" << std::endl;

    Real err_tot_l2 = 0.;
    Real norm_linfty_l2 =0.;
    Real err_l2_h1 = 0.;
    Real norm_l2_h1 = 0.;

  for(int n = 0; n < N_iter+2; n++){
    Real err_uh_l2= 0.;
    Real norm_u_l2 = 0.;
    Real err_en = 0.;
    Real norm_u_h1 = 0.;

    for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
        const Cell & T = Th->cell(iT);
        const Real & m_T = T.measure();
        const Point & x_T = T.center();
        const int m = T.numberOfFaces();
        // Compute local errors in L2-norm
        err_uh_l2 +=  m_T*std::pow( Sol_maille[n](iT) - sol.u(Temps[n],x_T), 2);
        norm_u_l2 +=  m_T*std::pow(  sol.u(Temps[n],x_T), 2);
        //compute local errors in [false] energy-like norm

        for(int iF_loc=0;iF_loc<m;iF_loc++ ){
            const int & iF=T.faceId(iF_loc);
            const Face & F = Th->face(iF);
            const Real & m_F = F.measure();
            const Point & x_F = F.barycenter();
            const Point & n_F= F.normal(x_T);
            const Real & d_F = inner_prod(n_F, x_F-x_T);

            err_en += (m_F/d_F)*std::pow(Sol_maille[n](iT)-Sol_faces[n](iF)-sol.u(Temps[n],x_T)+sol.u(Temps[n],x_F),2);
            norm_u_h1 += (m_F/d_F)*std::pow(sol.u(Temps[n],x_T)-sol.u(Temps[n],x_F),2);

        }// for iF_loc

    } // for iT
    err_uh_l2 = std::sqrt(err_uh_l2);
    norm_u_l2 = std::sqrt(norm_u_l2);
    //if(err_tot_l2< err_uh_l2){err_tot_l2 = err_uh_l2;}
    err_tot_l2 = max(err_tot_l2,err_uh_l2);
    norm_linfty_l2 = max(norm_linfty_l2, norm_u_l2);
    err_l2_h1 += D_t * err_en;
    norm_l2_h1 += D_t * norm_u_h1;


  } // for n

  err_l2_h1 = std::sqrt(err_l2_h1);
  norm_l2_h1 = std::sqrt(norm_l2_h1);

  std::cout << FORMAT(50) << "les erreurs affichées sont des erreurs relatives"  << std::endl;
  std::cout << FORMAT(50) << "err_uh - L^infty _ t L²_ x  rel: " << err_tot_l2 / norm_linfty_l2<< std::endl;
  std::cout << FORMAT(50) << "err_uh - L²_t H¹_x rel : " << err_l2_h1 / norm_l2_h1 << std::endl;

  Real m_vol = *min_element(Min_on_mesh.begin(), Min_on_mesh.end());
  Real m_edge = *min_element(Min_on_edge.begin(), Min_on_edge.end());
  std::cout << FORMAT(50) << "Minimum on mesh : " <<m_vol << std::endl;
  std::cout << FORMAT(50) << "Minimum on edge : " <<  m_edge << std::endl;
  std::cout << FORMAT(50) << "Proportion of negative unknowns: " <<  Nb_neg_unk/ Nb_unk << std::endl;
  std::cout << FORMAT(50) << "Number of negative unknowns: " <<  Nb_neg_unk  << std::endl;
  std::cout << FORMAT(50) << "Number (total) of unknowns: " <<   Nb_unk << std::endl;  
  std::cout << FORMAT(50) << "Number of negative cells unknowns: " <<   Nb_neg_cells_unk << std::endl;
  std::cout << FORMAT(50) << "Number of negative faces unknowns: " <<   Nb_neg_faces_unk << std::endl;
  std::cout << FORMAT(50) << "Number of linear resolutions: " <<  Nb_resol_lin << std::endl;
  std::cout << FORMAT(50) << "Time computation: " << c_solve.diff() +  c_facto.diff() + c_precomput.diff() + c_assembly.diff() << std::endl;
  

  vector<Real> Vec_data;
  Vec_data.push_back(h);
  Vec_data.push_back(err_tot_l2 / norm_linfty_l2);
  Vec_data.push_back(err_l2_h1 / norm_l2_h1 );
  Vec_data.push_back(m_vol);
  Vec_data.push_back(m_edge);
  Vec_data.push_back(Nb_neg_unk/ Nb_unk );
  Vec_data.push_back(Nb_resol_lin);

    std::cout << sep << std::endl;
    std::cout << FORMAT(50) << "DONE"  << std::endl;
    std::cout << sep << std::endl;

    return Vec_data;
}

vector<Real> omegaschema_mesh_harmo(const Real & epsilon, const int type_moy, const  Real & T, const int & N_iter, const string & Mesh_file ,const bool Visu ,Integer argc, char * argv[])
{
  std::string sep = "\n----------------------------------------\n";
  Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
  std::cout << "test1 ? "  << std::endl;

  // Read options and data
  GetPot options(argc, argv);

  std::string mesh_file = Mesh_file;
//  std::string mesh_file = "../meshes/mesh1_1.typ1";
//  if(options.search(2, "--mesh", "-m")) { mesh_file = options.next(""); }
//    std::cout << mesh_file  << std::endl;
  // Pretty-print floating point numbers
  std::cout.precision(2);
  std::cout.flags(std::ios_base::scientific);

  std::cout << "test2 ? "  << std::endl;
  // Create mesh
  std::shared_ptr<Mesh> Th(new Mesh());
  if(mesh_file.find("typ1")!=std::string::npos)
    Th->readFVCA5File(mesh_file.c_str());
  else if(mesh_file.find("dgm")!=std::string::npos)
    Th->readDGMFile(mesh_file.c_str());
  else {
    std::cerr << "Unknown mesh format" << std::endl;
    exit(1);
  }

  std::cout << "test3 ? "  << std::endl;

  Th->buildFaceGroups();
  std::cout << "MESH: " << mesh_file << std::endl;
  std::cout << FORMAT(50) << "num_cells" << Th->numberOfCells() << std::endl;
  std::cout << FORMAT(50) << "num_faces" << Th->numberOfFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws_cells" << Th->numberOfCells()  << std::endl;
  std::cout << FORMAT(50) << "num_unknws_faces" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws" << Th->numberOfCells() +  Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws_with_elimination" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;

  //------------------------------------------------------------------------------
  // Estimate mesh size
  std::cout << FORMAT(50) << "Temps final "<< T<< std::endl;
  std::cout<< FORMAT(50) << "Paramètre de stabilisation" << Eta << std::endl;

  Real h = Th->meshsize();
  std::cout << FORMAT(50) << "meshsize" << h << std::endl;
  Real D_t = T / (N_iter+1.);
  std::cout << FORMAT(50) << "time step size" << D_t << std::endl;



  //------------------------------------------------------------------------------
  // Exact solution

  ho::pho::Regular sol;

  // omega function and modified tensor and field

  //Real epsilon = 0.001;

  const FctContType omega = [epsilon,sol](const Point & x) -> Real {
    Real arg = - epsilon * sol.Phi(x);
    return std::exp(arg);
  };

  const DiffusivityType Lambda_omega = [sol, omega](const Point & x) -> TensorType {
    return omega(x) * sol.Lambda(x);
  };

  const FieldType V_omega = [sol, omega, epsilon](const Point & x) -> Eigen::Matrix<Real, 2, 1>  {
    return (1. - epsilon) * omega(x) * sol.V(x);
  };



  //------------------------------------------------------------------------------
  // Assemble matrix
  //------------------------------------------------------------------------------


  int nunkw = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
  int n_vol_unkw =  Th->numberOfCells();

  // Pre computation
  std::cout << "Pre-computation begins" << endl;
  common::chrono c_precomput;
  c_precomput.start();

  std::vector<vector<vector<Real>>> A;
  comp_A_elt_harmo(Th.get(), sol.Lambda, omega , type_moy, Eta, A);

  std::vector<vector<Real>> B;
  comp_B_elt(Th.get(), A, B );

  std::vector<Real> Alpha;
  comp_Alpha_elt(Th.get(), B, Alpha );

  std::vector<Real>  Diff_Int;
  comp_DiffInt(Th.get(),  Lambda_omega, Diff_Int );


  std::vector<vector<Real>> Conv_N;
  std::vector<vector<Real>> Conv_P;
  comp_Conv_elt(Th.get(), Diff_Int, V_omega,  Conv_N, Conv_P);



  std::vector<Real>  Conv_N_sum;
  std::vector<Real>  Conv_P_sum;
  comp_Alpha_elt(Th.get(), Conv_N , Conv_N_sum);
  comp_Alpha_elt(Th.get(), Conv_P , Conv_P_sum);

  //discrétisation donnée au bord (relevement) et donnée initiale
  Real Masse = 0.;
  SolutionVectorType U_0 = SolutionVectorType::Zero(n_vol_unkw);
  SolutionVectorType X_0 = SolutionVectorType::Zero(nunkw);
  for(int iT = 0; iT< Th->numberOfCells(); iT ++){
    const Cell & T = Th->cell(iT);
    const Real & m_T = T.measure();
    const Point & x_T = T.center();
    U_0(iT) = sol.u_0(x_T);
    Masse += m_T * U_0(iT);
  }//for iT

  std::cout << FORMAT(50) << "Discrete mass"  << Masse << std::endl;

    for(int iF = 0; iF < nunkw ; iF ++){
        const Face & F = Th->face(iF);
        const Point & x_F = F.barycenter();
        X_0(iF) = sol.u_0(x_F);
    }// for iF_loc


  c_precomput.stop();
  cout << "Pre-computation ends" << endl;
  std::cout << FORMAT(50) << "time_precomputation"  << c_precomput.diff() << std::endl;


    std::vector<SolutionVectorType> Sol_faces;
    std::vector<SolutionVectorType> Sol_maille;
    std::vector<Real> Min_on_mesh;
    std::vector<Real> Min_on_edge;
    int Nb_neg_unk = 0;
    int Nb_unk = 0;
    int Nb_neg_cells_unk = 0;
    int Nb_neg_faces_unk =0;
    std::vector<Real> Temps;

    Sol_maille.push_back(U_0);
    Sol_faces.push_back(X_0);
    Temps.push_back(0.);

    common::chrono c_assembly;
    c_assembly.start();
    cout << "matrix assembly begins" << endl;



    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_edge_max =Th->maximumNumberOfFaces();

    //compute h -> u matrixes
    std::vector<TripletType> Triplets_Mat_mass;
    Triplets_Mat_mass.reserve(n_vol);
    std::vector<TripletType> Triplets_D_vol;
    Triplets_D_vol.reserve(n_vol);
    std::vector<TripletType> Triplets_D_edge;
    Triplets_D_edge.reserve(n_edge);

    for(int iT = 0; iT < n_vol ; iT ++){
        const Cell & T = Th->cell(iT);
        const Point & x_T = T.center();
        const Real & omega_T = omega(x_T);
        const Real & m_T = T.measure();
        Triplets_D_vol.push_back(TripletType(iT, iT, 1./omega_T));
        Triplets_Mat_mass.push_back(TripletType(iT, iT, m_T));
    } //for iT

    SparseMatrixType Mat_mass(n_vol, n_vol);
    Mat_mass.setFromTriplets(Triplets_Mat_mass.begin(), Triplets_Mat_mass.end());
    Mat_mass = (1./D_t)*Mat_mass;
    SparseMatrixType D_vol(n_vol, n_vol);
    D_vol.setFromTriplets(Triplets_D_vol.begin(), Triplets_D_vol.end());

    for(int iF = 0; iF < n_edge ; iF ++){
        const Face & F = Th->face(iF);
        const Point & x_F = F.barycenter();
        const Real omega_F = omega(x_F);
        Triplets_D_edge.push_back(TripletType(iF, iF, 1./omega_F));
    } //for iF

    SparseMatrixType D_edge(n_edge, n_edge);
    D_edge.setFromTriplets(Triplets_D_edge.begin(), Triplets_D_edge.end());



    // compute triplets ( h linear system)
    std::vector<TripletType> Triplets_invM1;
    Triplets_invM1.reserve(n_vol);
    std::vector<TripletType> Triplets_M2;
    Triplets_M2.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_M3;
    Triplets_M3.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_M4;
    Triplets_M4.reserve(n_vol* n_edge_max * n_edge_max);

    triplets_invM1andM2_evol(Th.get(), D_t, omega,  Alpha, B, Conv_N_sum,  Conv_P,
    Triplets_invM1,  Triplets_M2);

    triplets_M3andM4(Th.get(),  B, A, Conv_N, Conv_P,
    Triplets_M3, Triplets_M4);

    //compute matrix
    SparseMatrixType invM1(n_vol, n_vol);
    invM1.setFromTriplets(Triplets_invM1.begin(), Triplets_invM1.end());
    SparseMatrixType M2(n_vol, n_edge);
    M2.setFromTriplets(Triplets_M2.begin(), Triplets_M2.end());
    M2 = M2 * D_edge;
    SparseMatrixType M3(n_edge, n_vol);
    M3.setFromTriplets(Triplets_M3.begin(), Triplets_M3.end());
    M3 = M3 * D_vol;
    SparseMatrixType M4(n_edge, n_edge);
    M4.setFromTriplets(Triplets_M4.begin(), Triplets_M4.end());
    M4 =M4 *D_edge;

    SolutionVectorType  Sec_m_vol = SolutionVectorType::Zero(n_vol);

    comp_sec_memb_Neumann(Th.get()  , sol.f, Sec_m_vol );



    SparseMatrixType GLOBAL_LHS =  (M4 -M3*invM1 * M2) ;
    Eigen::VectorXd GLOBAL_RHS =  ( - M3*invM1 * Sec_m_vol);

    c_assembly.stop();
    cout << "Matrix assembly ends" << endl;
    std::cout << FORMAT(50) << "time_assembly"  << c_assembly.diff() << std::endl;

    // Solve linear system and reconstruct mesh unknowns

    common::chrono c_facto;
    c_facto.start();

    cout << "Linear solving begins" << endl;

    SolutionVectorType X = SolutionVectorType::Zero(nunkw);
    SolutionVectorType U = SolutionVectorType::Zero(n_vol_unkw);

    Eigen::SparseLU<SparseMatrixType> solver_evo;
    // Compute the ordering permutation vector from the structural pattern of ...
    solver_evo.analyzePattern(GLOBAL_LHS);
    std::cout << FORMAT(50) << "analyse pattern OK"  << std::endl;
    // Compute the numerical factorization
    solver_evo.factorize(GLOBAL_LHS);
    c_facto.stop();
    std::cout << FORMAT(50) << "factorisation etat stationnaire  OK"  << std::endl;
    std::cout << FORMAT(50) << "time_facto"  << c_facto.diff() << std::endl;


    //Use the factors to solve the linear system
    int Nb_resol_lin = 0;
    common::chrono c_solve;
    c_solve.start();
    for(int n = 0 ; n<  N_iter +1; n ++){

        if(n%200==0){std::cout << "Calcul de la "<< n+1 << "-eme  itération" << std::endl; }
        SolutionVectorType X = SolutionVectorType::Zero(nunkw);
        SolutionVectorType U = SolutionVectorType::Zero(n_vol_unkw);

        Eigen::VectorXd GLOBAL_RHS_n =  - M3*invM1 * ( Sec_m_vol + Mat_mass * Sol_maille[n] );

        X = (solver_evo.solve(GLOBAL_RHS_n));
        Nb_resol_lin += 1;
        U = invM1 * (Sec_m_vol + Mat_mass * Sol_maille[n] - M2*X);
        Sol_faces.push_back(X);
        Sol_maille.push_back(U);
        Min_on_mesh.push_back(U.minCoeff());
        Min_on_edge.push_back(X.minCoeff());

        for(int iT =0 ; iT < n_vol; iT ++){
            if(U(iT)< 0.){
              Nb_neg_cells_unk +=1;
              Nb_neg_unk += 1;
            } //if 
        } //for iT, count the numberof negative mesh unk

        for(int iF =0 ; iF < n_edge; iF ++){
            if(X(iF)< 0.){
            Nb_neg_faces_unk +=1;
            Nb_neg_unk += 1;}
        } //for iF, count the numberof negative faces unk


        Nb_unk += n_vol + n_edge;


        Temps.push_back((n +1.) * D_t);
    }

    c_solve.stop();
    std::cout << FORMAT(50) << "fin calcul "  << std::endl;
    std::cout << FORMAT(50) << "time_solve"  << c_solve.diff() << std::endl;


    //Creation etat stationnaire discret
    std::cout << "Discretisation of steady state begins" << endl;

    Real rho = 0;
    for(int iT =0 ; iT < n_vol_unkw ; iT ++){
        const Cell & T = Th->cell(iT);
        const Real & m_T = T.measure();
        const Point & x_T = T.center();
        rho += m_T * exp(-sol.Phi(x_T));
    }//for iT

  rho = Masse/rho;

    SolutionVectorType U_infty = SolutionVectorType::Zero(n_vol_unkw);
    SolutionVectorType X_infty = SolutionVectorType::Zero(nunkw);
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Point & x_T = T.center();
        U_infty(iT) = rho*exp(-sol.Phi(x_T));
        }//for iT

      for(int iF = 0; iF< nunkw;iF++){
          const Face & F = Th->face(iF);
          const Point & x_F = F.barycenter();
          X_infty(iF) = rho*exp(-sol.Phi(x_F));
          } //for iF

    std::cout << "Discretisation of steady state ends" << endl;

    std::cout << sep << std::endl;
    std::cout << FORMAT(50) << "Errors"  << std::endl;
    std::cout << sep << std::endl;

    //Entropies et dissipations

    std::cout << sep << "Calcul des entropies et dissipation" << std::endl;


    std::vector<Real> Diff_L2_square;
    std::vector<Real> Diff_L1_square;
    std::vector<Real> Diff_H1;

    for(int n = 0;n<  N_iter+2; n ++)
    {
        Real dif_h1 =0.;
        Real diff_l2 = 0.;
        Real diff_l1 = 0.;

        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
            const Cell & T = Th->cell(iT);
            const Real m_T = T.measure();
            const Point x_T = T.center();
            const int m = T.numberOfFaces();

            diff_l2+= m_T*std::pow( U_infty(iT) -Sol_maille[n](iT) , 2);
            diff_l1+= m_T*std::fabs( U_infty(iT) -Sol_maille[n](iT));


	// Compute local error in energy-like norm (discrete H^1-norm)
            for(int iF_loc=0;iF_loc<m;iF_loc++ ){
                const int iF=T.faceId(iF_loc);
                const Face & F = Th->face(iF);
                const Real m_F = F.measure();
                const Point x_F = F.barycenter();
                const Point n_F= F.normal(x_T);
                const Real d_F = inner_prod(n_F, x_F-x_T);

                dif_h1 += (m_F/d_F)*std::pow(U_infty(iT)-X_infty(iF)-Sol_maille[n](iT) + Sol_faces[n](iF),2);
            }// for iF_loc
        } // for iT

    Diff_L2_square.push_back(diff_l2);
    Diff_L1_square.push_back(diff_l1*diff_l1);
    Diff_H1.push_back(dif_h1);

    } // for n

   std::cout << "Entropies et dissipations calculées"  << std::endl;

   const int mod =round(N_iter / nb_pts_in_graph) +1;



   std::ofstream myfile;
      myfile.open ("temps_long_EFharmo.csv");
      myfile << "Première cellule premiere colonne .\n";
      myfile <<"Description "<<"," <<"Convection diffusion, omega schéma harmonique.\n";
      myfile <<"Epsilon"<<"," << epsilon << endl;
      myfile <<"Eta"<<"," << Eta  << endl;
      myfile <<"Pas de temps"<<"," << D_t  << endl;
      myfile <<"Nombre d'itérations "<<"," << N_iter  << endl;
      myfile <<"Maillage"<<"," << mesh_file  << endl;
      myfile <<"Finesse de maillage"<<"," << h  << endl;
      myfile <<"" << endl;
      myfile << "Temps,Diff_L1_square,Diff_L2_square,Diff_H1_square,\n";
      for(int n=0; n< N_iter+2; n ++){
        myfile << Temps[n] <<","<<Diff_L1_square[n] <<"," << Diff_L2_square[n] << ","<< Diff_H1[n]  << endl;
      }
      myfile.close();

      std::ofstream myfile2;
      myfile2.open ("tps_EFharmo");
      myfile2 << "Temps Diff_L1_square Diff_L2_square Diff_H1_square Diff_L1 Diff_L2 \n";
      for(int n=0; n< N_iter+2; n ++){
        if(n % mod ==0){
            myfile2 << Temps[n] <<" "<<Diff_L1_square[n] <<" " << Diff_L2_square[n] << " "<< Diff_H1[n] << " "<< std::sqrt(Diff_L1_square[n]) <<" " << std::sqrt(Diff_L2_square[n]) << endl;
        }// if n % 100 ==0
      }//for n
      myfile2.close();

    std::cout <<  "Enregistrement fichier Entropies" << std::endl;

    if(Visu){
        std::cout << sep << "Fichiers visualisation" << std::endl;

        std::vector<Eigen::VectorXd> coeffs_u_infty(Th->numberOfCells());
        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
            Eigen::VectorXd uh_F_infty (1);
            uh_F_infty(0) =U_infty(iT);
            coeffs_u_infty[iT] = uh_F_infty;
        }
      postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>("output/uh_infty.vtu", Th.get(), coeffs_u_infty);

      std::cout <<  "Enregistrement fichier visualisation - etat stationnaire " << std::endl;

      for(int i=0; i< N_iter+2; i++ ){
        if(i % mod ==0){
            std::vector<Eigen::VectorXd> coeffs_u(Th->numberOfCells());
            for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
              Eigen::VectorXd uh_F_end (1);
              uh_F_end(0) =Sol_maille[i](iT);
              coeffs_u[iT] = uh_F_end;
            } // for iT

            char str[20] = {0};
            std::sprintf(str, "%d", i);
            char name_c[40];
            strcpy(name_c,"output/uh_");
            strcat(name_c, str);
            strcat(name_c,".vtu");
            postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>(name_c, Th.get(), coeffs_u);
        }


      } //for i


      std::cout <<  "Enregistrement fichier visualisation - evolutif " << std::endl;

      std::vector<Eigen::VectorXd> coeffs_u_end(Th->numberOfCells());
      for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
        Eigen::VectorXd uh_F_end (1);
        uh_F_end(0) =Sol_maille[N_iter+1](iT);
        coeffs_u_end[iT] = uh_F_end;
      }
      postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>("output/uh_end.vtu", Th.get(), coeffs_u_end);
  }
    //Calcul erreur cas test et vérification, norme L^infty_t L²_x

    std::cout << sep << "Calcul erreurs - cas test" << std::endl;

    Real err_tot_l2 = 0.;
    Real norm_linfty_l2 =0.;
    Real err_l2_h1 = 0.;
    Real norm_l2_h1 = 0.;

  for(int n = 0; n < N_iter+2; n++){
    Real err_uh_l2= 0.;
    Real norm_u_l2 = 0.;
    Real err_en = 0.;
    Real norm_u_h1 = 0.;

    for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
        const Cell & T = Th->cell(iT);
        const Real & m_T = T.measure();
        const Point & x_T = T.center();
        const int m = T.numberOfFaces();
        // Compute local errors in L2-norm
        err_uh_l2 +=  m_T*std::pow( Sol_maille[n](iT) - sol.u(Temps[n],x_T), 2);
        norm_u_l2 +=  m_T*std::pow(  sol.u(Temps[n],x_T), 2);
        //compute local errors in [false] energy-like norm

        for(int iF_loc=0;iF_loc<m;iF_loc++ ){
            const int & iF=T.faceId(iF_loc);
            const Face & F = Th->face(iF);
            const Real & m_F = F.measure();
            const Point & x_F = F.barycenter();
            const Point & n_F= F.normal(x_T);
            const Real & d_F = inner_prod(n_F, x_F-x_T);

            err_en += (m_F/d_F)*std::pow(Sol_maille[n](iT)-Sol_faces[n](iF)-sol.u(Temps[n],x_T)+sol.u(Temps[n],x_F),2);
            norm_u_h1 += (m_F/d_F)*std::pow(sol.u(Temps[n],x_T)-sol.u(Temps[n],x_F),2);

        }// for iF_loc

    } // for iT
    err_uh_l2 = std::sqrt(err_uh_l2);
    norm_u_l2 = std::sqrt(norm_u_l2);
    //if(err_tot_l2< err_uh_l2){err_tot_l2 = err_uh_l2;}
    err_tot_l2 = max(err_tot_l2,err_uh_l2);
    norm_linfty_l2 = max(norm_linfty_l2, norm_u_l2);
    err_l2_h1 += D_t * err_en;
    norm_l2_h1 += D_t * norm_u_h1;


  } // for n

  err_l2_h1 = std::sqrt(err_l2_h1);
  norm_l2_h1 = std::sqrt(norm_l2_h1);

  std::cout << FORMAT(50) << "les erreurs affichées sont des erreurs relatives"  << std::endl;
  std::cout << FORMAT(50) << "err_uh - L^infty _ t L²_ x  rel: " << err_tot_l2 / norm_linfty_l2<< std::endl;
  std::cout << FORMAT(50) << "err_uh - L²_t H¹_x rel : " << err_l2_h1 / norm_l2_h1 << std::endl;

  Real m_vol = *min_element(Min_on_mesh.begin(), Min_on_mesh.end());
  Real m_edge = *min_element(Min_on_edge.begin(), Min_on_edge.end());
  std::cout << FORMAT(50) << "Minimum on mesh : " <<m_vol << std::endl;
  std::cout << FORMAT(50) << "Minimum on edge : " <<  m_edge << std::endl;
  std::cout << FORMAT(50) << "Proportion of negative unknowns: " <<  Nb_neg_unk/ Nb_unk << std::endl;
  std::cout << FORMAT(50) << "Number of negative unknowns: " <<  Nb_neg_unk  << std::endl;
  std::cout << FORMAT(50) << "Number (total) of unknowns: " <<   Nb_unk << std::endl;
  std::cout << FORMAT(50) << "Number of negative cells unknowns: " <<   Nb_neg_cells_unk << std::endl;
  std::cout << FORMAT(50) << "Number of negative faces unknowns: " <<   Nb_neg_faces_unk << std::endl;
  std::cout << FORMAT(50) << "Number of linear resolutions: " <<  Nb_resol_lin << std::endl;
  std::cout << FORMAT(50) << "Time computation: " << c_solve.diff() +  c_facto.diff() + c_precomput.diff() + c_assembly.diff() << std::endl;

  vector<Real> Vec_data;
  Vec_data.push_back(h);
  Vec_data.push_back(err_tot_l2 / norm_linfty_l2);
  Vec_data.push_back(err_l2_h1 / norm_l2_h1 );
  Vec_data.push_back(m_vol);
  Vec_data.push_back(m_edge);
  Vec_data.push_back(Nb_neg_unk/ Nb_unk );
  Vec_data.push_back(Nb_resol_lin);

    std::cout << sep << std::endl;
    std::cout << FORMAT(50) << "DONE"  << std::endl;
    std::cout << sep << std::endl;

    return Vec_data;
}


vector<Real> non_linear_mesh(const  Real & T_end ,const  int & Nb_iter,const string & Mesh_file , const bool Visu , Integer argc, char * argv[])
{

  std::string sep = "\n----------------------------------------\n";
  Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
  std::cout << "test1 ? "  << std::endl;

  // Read options and data
  GetPot options(argc, argv);

  std::string mesh_file = Mesh_file;


  // Pretty-print floating point numbers
  std::cout.precision(2);
  std::cout.flags(std::ios_base::scientific);

  std::cout << "test2 ? "  << std::endl;
  // Create mesh
  std::shared_ptr<Mesh> Th(new Mesh());
  if(mesh_file.find("typ1")!=std::string::npos)
    Th->readFVCA5File(mesh_file.c_str());
  else if(mesh_file.find("dgm")!=std::string::npos)
    Th->readDGMFile(mesh_file.c_str());
  else {
    std::cerr << "Unknown mesh format" << std::endl;
    exit(1);
  }

  std::cout << "test3 ? "  << std::endl;

  Th->buildFaceGroups();
  std::cout << "MESH: " << mesh_file << std::endl;
  std::cout << FORMAT(50) << "num_cells" << Th->numberOfCells() << std::endl;
  std::cout << FORMAT(50) << "num_faces" << Th->numberOfFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws_cells" << Th->numberOfCells()  << std::endl;
  std::cout << FORMAT(50) << "num_unknws_faces" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws" << Th->numberOfCells() +  Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws_with_elimination" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;

  //------------------------------------------------------------------------------
  //Acquisition donnée pas de temps
  //int Nb_iter;
  //cout << "Nombre d'itérations pour le calcul de la solution en temps ?" << endl;
  //cin >> Nb_iter;

  //------------------------------------------------------------------------------
  // Estimate mesh size
  std::cout << FORMAT(50) << "Temps final "<< T_end << std::endl;
  std::cout<< FORMAT(50) << "Paramètre de stabilisation" << Eta << std::endl;

  Real h = Th->meshsize();
  std::cout << FORMAT(50) << "meshsize" << h << std::endl;
  Real D_t = T_end / (1 +Nb_iter);


  //------------------------------------------------------------------------------
  // Exact solution

  ho::pho::Regular sol;

  //------------------------------------------------------------------------------
  // Assemble matrix
  //------------------------------------------------------------------------------


  int nunkw = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
  int n_vol_unkw =  Th->numberOfCells();
  
  common::chrono c_time_cost;
  c_time_cost.start();
  
  // Pre computation
  std::cout << "Pre-computation begins" << endl;
  common::chrono c_precomput;
  c_precomput.start();

  std::vector<vector<vector<Real>>> A;
  comp_A_elt(Th.get(), sol.Lambda, Eta, A);

  std::vector<vector<Real>> B;
  comp_B_elt(Th.get(), A, B );

  std::vector<Real> Alpha;
  comp_Alpha_elt(Th.get(), B, Alpha );

  std::vector<vector<LocalVectorType>> Flux;
  precomp_flux(Th.get(), A, B, Flux );

  std::vector<LocalVectorType>  Sum_Flux;
  precomp_Somme_flux(Th.get(), B, Sum_Flux);

  std::vector<LocalVectorType>  Reconstruction;
  precomp_reconstruc(Th.get(),Reconstruction);

  //Discrete potential
  SolutionVectorType Phi_edge = SolutionVectorType::Zero(nunkw);
  SolutionVectorType Phi_vol = SolutionVectorType::Zero(n_vol_unkw);

    for(int iT=0; iT< n_vol_unkw; iT ++){
        const Cell & T = Th->cell(iT);
        const Point & x_T = T.center();
        Phi_vol(iT) = sol.Phi(x_T);
        } // for iT

    for(int iF=0 ; iF< nunkw ; iF++){
      const Face & F = Th->face(iF);
      const Point & x_F = F.barycenter();
      Phi_edge(iF) = sol.Phi(x_F);
  }//for iF

  std::vector<vector<Real>>  Flux_Phi;
  comp_flux_potential(Th.get(), Flux, Phi_vol , Phi_edge , Flux_Phi);

  std::vector<Real>  Sum_Flux_Phi;
  comp_Sumflux_potential(Th.get(), Sum_Flux, Phi_vol,  Phi_edge, Sum_Flux_Phi);

  std::vector<LocalMatrixType>  Mat_loc;
  comp_Mat_loc_bilin(Th.get(), Flux,  Sum_Flux, Mat_loc);
  std::cout << FORMAT(50) << "Matrice locale "  << std::endl;
  std::cout  << Mat_loc[0]  << std::endl;


  c_precomput.stop();
  cout << "Pre-computation ends" << endl;
  std::cout << FORMAT(50) << "time_precomputation"  << c_precomput.diff() << std::endl;

  //Creation condition initiale discrete
   std::cout << "Discretisation of initial data begins" << endl;

  Real Masse = 0.;

  SolutionVectorType U_0 = SolutionVectorType::Zero(n_vol_unkw);
  SolutionVectorType X_0 = SolutionVectorType::Zero(nunkw);
  for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
    const Cell & T = Th->cell(iT);
    const Real & m_T = T.measure();
    const Point & x_T = T.center();
    U_0(iT) = sol.u_0(x_T);
    Masse += m_T * U_0(iT);
    }//for iT

  std::cout << FORMAT(50) << "Discrete mass"  << Masse << std::endl;

  for(int iF = 0; iF< nunkw;iF++){
      const Face & F = Th->face(iF);
      const Point & x_F = F.barycenter();
      X_0(iF) = sol.u_0(x_F);
      }

  std::vector<SolutionVectorType> Sol_faces;
  std::vector<SolutionVectorType> Sol_maille;
  std::vector<Real> Min_on_mesh;
  std::vector<Real> Min_on_edge;
  //Sol_maille.reserve(2*Nb_iter);
  std::vector<Real> Temps;
  std::vector<int> Nb_iter_Newton;

  Sol_maille.push_back(U_0);
  Sol_faces.push_back(X_0);
  Temps.push_back(0.);
  //Min_on_mesh.push_back(U_0.minCoeff());
  //Min_on_edge.push_back(X_0.minCoeff());
  Nb_iter_Newton.push_back(0.);

  std::cout << "Discretisation of initial data ends" << endl;
  int n_iter = 0;
  int Mod_pastemps = 0;
  bool iter_is_pos;
  bool res_are_small;
  bool too_much_iter;
  bool Newton_is_CV;
  bool Newton_can_continue;

  int Nb_resol_sys_lin =0;

  while(Temps.back()<T_end- std::pow(10.,-14)){

      Mod_pastemps = max(0, Mod_pastemps -1 );
      n_iter += 1;

      do{ //boucle newton temps adaptatif
          std::cout << FORMAT(50) << "n_iter "<< n_iter << std::endl;
          SolutionVectorType R_vol = SolutionVectorType::Zero(n_vol_unkw);
          SolutionVectorType R_edge = SolutionVectorType::Zero(nunkw);
          //std::cout << FORMAT(50) << "Sol_maille"<< Sol_maille[n_iter]<< std::endl;

          SolutionVectorType U_ini = proj_UKN_vol(Th.get(), s_pos,  Sol_maille[n_iter-1]);
          SolutionVectorType X_ini = proj_UKN_edge(Th.get(), s_pos,  Sol_faces[n_iter-1]);


          Real pas_temps = D_t / (std::pow(2., Mod_pastemps));

          int k = 0;
          std::cout << FORMAT(50) << "Newton go ! pas temps " << pas_temps << endl;
          do{ //boucle Newton
            Real Norm_G = comp_sol_iter_newton(Th.get(), pas_temps, Sol_maille[n_iter-1] , Sol_faces[n_iter-1],
                A, B, Alpha, Reconstruction, Flux , Flux_Phi, Sum_Flux, Sum_Flux_Phi,
                U_ini , X_ini, R_vol , R_edge );

            Nb_resol_sys_lin +=1; //compte nombre resolution systemes lineaires


            Real Norm_R_infty = std::max(R_vol.lpNorm<Eigen::Infinity>() , R_edge.lpNorm<Eigen::Infinity>());

            U_ini = R_vol + U_ini;
            X_ini = R_edge + X_ini;

            Real Norm_R_rel = (R_vol.lpNorm<1>() + R_edge.lpNorm<1>() ) /(U_ini.lpNorm<1>() + X_ini.lpNorm<1>());
            Real min_iter =min( U_ini.minCoeff() , X_ini.minCoeff());

            iter_is_pos = (min_iter > 0.);
            //res_are_small = (Norm_R_infty < s_R && Norm_G < s_G && Norm_R_rel < s_R_rel );
            res_are_small = (Norm_R_rel < s_R_rel );
            k += 1;
            too_much_iter=(k>s_iter);

            Newton_is_CV = (!too_much_iter) && res_are_small && iter_is_pos;
            Newton_can_continue = iter_is_pos && (! res_are_small) && (! too_much_iter);



          //std::cout << "iter Newton OK" << endl;
          //std::cout << FORMAT(50) << "Sol_maille"  << U_ini<< std::endl;
            //std::cout << FORMAT(50) << "N_G"  << Norm_G<< std::endl;
          //std::cout << FORMAT(50) << "N_G _inf"  << << std::endl;
            //std::cout << FORMAT(50) << "N_R rel"  <<  Norm_R_rel << std::endl;
            //std::cout << FORMAT(50) << "N_R_infty"  <<  Norm_R_infty << std::endl;

            //std::cout << FORMAT(50) << "Min_iteration"  <<  min_iter << std::endl;
            //std::cout << FORMAT(50) << "Iter"  <<  k  << "\n" << std::endl;
          //std::cout << FORMAT(50) << "U "  <<  U_ini  << "\n" << std::endl;



            //}while( (k < s_iter) && (Norm_G > s_G || Norm_R > s_R )) ; //boucle Newton
          }while(Newton_can_continue) ; //boucle Newton

          std::cout << FORMAT(50) << "Sortie iteration Newton \n" << std::endl;
          //Newton_CV = (k == s_iter);
          std::cout << FORMAT(50) << "Valeur Newton_CV" << Newton_is_CV << std::endl;

          if(Newton_is_CV){
            Temps.push_back(Temps.back()+ pas_temps);
            std::cout << FORMAT(50) << "Calcul sol au temps" << Temps.back() << "OK"<< std::endl;

            Nb_iter_Newton.push_back(k);
            Sol_maille.push_back(U_ini);
            Sol_faces.push_back(X_ini);
            Min_on_mesh.push_back(U_ini.minCoeff());
            Min_on_edge.push_back(X_ini.minCoeff());
          } //if Newton CV

          else{Mod_pastemps += 1;}

        }while(!Newton_is_CV); // boucle newton avec pas temps adaptatif

        std::cout << "temps +1 " << std::endl;

        cout << FORMAT(50) << "Temps" << Temps.back() << endl;
        cout << FORMAT(50) << "n_iter" << n_iter << endl;
  } //boucle temps


  std::cout << "Fin du calcul de la solution  du problème évolutif" << std::endl;
  std::cout << "Nombre d'itérations en temps "  << n_iter << std::endl;  
  c_time_cost.stop();



  //Creation etat stationnaire discret
   std::cout << "Discretisation of steady state begins" << endl;

  Real rho = 0;
  for(int iT =0 ; iT < n_vol_unkw ; iT ++){
    const Cell & T = Th->cell(iT);
    const Real & m_T = T.measure();
    const Point & x_T = T.center();
    rho += m_T * exp(-sol.Phi(x_T));
  }//for iT

  rho = Masse/rho;

  SolutionVectorType U_infty = SolutionVectorType::Zero(n_vol_unkw);
  SolutionVectorType X_infty = SolutionVectorType::Zero(nunkw);
  for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
    const Cell & T = Th->cell(iT);
    const Point & x_T = T.center();
    U_infty(iT) = rho*exp(-sol.Phi(x_T));
    }//for iT

  for(int iF = 0; iF< nunkw;iF++){
      const Face & F = Th->face(iF);
      const Point & x_F = F.barycenter();
      X_infty(iF) = rho*exp(-sol.Phi(x_F));
      } //for iF

  std::cout << "Discretisation of steady state ends" << endl;



  //------------------------------------------------------------------------------


  std::cout << sep << std::endl;
  std::cout << FORMAT(50) << "Erreurs"  << std::endl;
  std::cout << sep << std::endl;
  //------------------------------------------------------------------------------
  // Compute errors

   std::cout << sep << "Calcul des entropies et dissipation" << std::endl;

   std::vector<Real> Entropie;
   std::vector<Real> Dissipation;
   std::vector<Real> Diff_L2_square;
   std::vector<Real> Diff_L1_square;
   std::vector<Real> Diff_H1;

   for(int n = 0;n<  n_iter+1; n ++)
    {
      Real entro = 0;
      Real dissip = 0;
      Real dif_h1 =0;
      Real diff_l2 = 0.;
      Real diff_l1 = 0.;

      for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
	const Cell & T = Th->cell(iT);
	const Real m_T = T.measure();
	const Point x_T = T.center();
	const int m = T.numberOfFaces();
	const LocalVectorType g_T = loc_logvec(Th.get(),iT,Sol_maille[n],Sol_faces[n]) + loc_vec(Th.get(),iT,Phi_vol,Phi_edge);

	entro += m_T * Sol_maille[n](iT) * log( Sol_maille[n](iT) / U_infty(iT));
	diff_l2+= m_T*std::pow( U_infty(iT) -Sol_maille[n](iT) , 2);
	diff_l1+= m_T*std::fabs( U_infty(iT) -Sol_maille[n](iT));
	dissip += Reconstruction[iT].dot(loc_vec(Th.get(),iT,Sol_maille[n],Sol_faces[n])) *  ( ( Mat_loc[iT] * g_T).dot(g_T));

	// Compute local error in energy-like norm (discrete H^1-norm)
	for(int iF_loc=0;iF_loc<m;iF_loc++ ){
	  const int iF=T.faceId(iF_loc);
	  const Face & F = Th->face(iF);
	  const Real m_F = F.measure();
	  const Point x_F = F.barycenter();
	  const Point n_F= F.normal(x_T);
	  const Real d_F = inner_prod(n_F, x_F-x_T);

	  dif_h1 += (m_F/d_F)*std::pow(U_infty(iT)-X_infty(iF)-Sol_maille[n](iT) + Sol_faces[n](iF),2);

	}// for iF_loc
      } // for iT

      Diff_L2_square.push_back(diff_l2);
      Diff_L1_square.push_back(diff_l1*diff_l1);
      Dissipation.push_back(dissip);
      Entropie.push_back(entro);
      Diff_H1.push_back(dif_h1);

    } // for n

   std::cout << "Entropies et dissipations calculées"  << std::endl;

    const int mod =round(n_iter / nb_pts_in_graph) +1;

   std::ofstream myfile;
      myfile.open ("temps_long_nonlin.csv");
      myfile << "This is the first cell in the first column.\n";
      myfile <<"Description "<<"," <<"Convection diffusion, schéma non linéaire.\n";
      myfile <<"Eta"<<"," << Eta  << endl;
      myfile <<"Pas de temps ref"<<"," << D_t  << endl;
      myfile <<"Nombre d'itérations prévu"<<"," << Nb_iter  << endl;
      myfile <<"Nombre d'itérations effectif"<<"," << n_iter  << endl;
      myfile <<"Maillage"<<"," << mesh_file  << endl;
      myfile <<"Finesse de maillage"<<"," << h  << endl;
      myfile <<"" << endl;
      myfile << "Temps,Diff_L1_square,Entropie,Dissipation,Diff_L2_square,Diff_H1_square,Nombre_iterations,\n";
      for(int n=0; n< n_iter+1; n ++){
        myfile << Temps[n] <<","<<Diff_L1_square[n] <<","<<Entropie[n] << "," << Dissipation[n] << "," << Diff_L2_square[n] << ","<< Diff_H1[n] << ","<< Nb_iter_Newton[n] << endl;
      }
      myfile.close();

      std::ofstream myfile2;
      myfile2.open ("tps_nonlin");
      myfile2 << "Temps Diff_L1_square Entropie Dissipation Diff_L2_square Diff_H1_square Diff_L1 Diff_L2 \n";
      for(int n=0; n< n_iter+1; n ++){
        if(n % mod ==0){
            myfile2 << Temps[n] <<" "<<Diff_L1_square[n] <<" "<<Entropie[n] << " " << Dissipation[n] << " " << Diff_L2_square[n] << " "<< Diff_H1[n] << " "<< std::sqrt(Diff_L1_square[n]) <<" " << std::sqrt(Diff_L2_square[n]) << endl;
        }// if n % 100 ==0
      }//for n
      myfile2.close();

    std::cout <<  "Enregistrement fichier Entropies" << std::endl;

    if(Visu){
  std::vector<Eigen::VectorXd> coeffs_u_infty(Th->numberOfCells());
  for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
    Eigen::VectorXd uh_F_infty (1);
    uh_F_infty(0) =U_infty(iT);
    coeffs_u_infty[iT] = uh_F_infty;
  }
  postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>("output/nonlin_uh_infty.vtu", Th.get(), coeffs_u_infty);

  std::cout <<  "Enregistrement fichier visualisation - etat stationnaire " << std::endl;

  for(int i=0; i< n_iter+1; i++ ){
    if(i % mod ==0){
    std::vector<Eigen::VectorXd> coeffs_u(Th->numberOfCells());
    for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
      Eigen::VectorXd uh_F_end (1);
      uh_F_end(0) =Sol_maille[i](iT);
      coeffs_u[iT] = uh_F_end;
  } // for iT

    char str[20] = {0};
    std::sprintf(str, "%d", i);
    char name_c[35];
    strcpy(name_c,"output/nonlin_uh_");
    strcat(name_c, str);
    strcat(name_c,".vtu");
    postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>(name_c, Th.get(), coeffs_u);
    }


  } //for i


  std::cout <<  "Enregistrement fichier visualisation - evolutif " << std::endl;

  std::vector<Eigen::VectorXd> coeffs_u_end(Th->numberOfCells());
  for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
    Eigen::VectorXd uh_F_end (1);
    uh_F_end(0) =Sol_maille[n_iter](iT);
    coeffs_u_end[iT] = uh_F_end;
  }
  postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>("output/nonlin_uh_end.vtu", Th.get(), coeffs_u_end);
 }

  //Calcul erreur cas test et vérification, norme L^infty_t L²_x

  std::cout << sep << "Calcul erreurs - cas test" << std::endl;

  Real err_tot_l2 = 0.;
  Real norm_linfty_l2 = 0.;
  Real err_l2_h1 = 0.;
  Real norm_l2_h1 = 0.;

  for(int n = 0; n < n_iter+1; n++){
    Real err_uh_l2= 0.;
    Real norm_u_l2 = 0.;
    Real err_en = 0.;
    Real norm_u_h1 = 0.;

    for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
      const Cell & T = Th->cell(iT);
      const Real & m_T = T.measure();
      const Point & x_T = T.center();
      const int m = T.numberOfFaces();
      // Compute local errors in L2-norm
      err_uh_l2 +=  m_T*std::pow( Sol_maille[n](iT) - sol.u(Temps[n],x_T), 2);
      norm_u_l2 +=  m_T*std::pow(  sol.u(Temps[n],x_T), 2);

      //compute local errors in [false] energy-like norm
      if(n<n_iter){
        for(int iF_loc=0;iF_loc<m;iF_loc++ ){
            const int iF=T.faceId(iF_loc);
            const Face & F = Th->face(iF);
            const Real & m_F = F.measure();
            const Point & x_F = F.barycenter();
            const Point & n_F= F.normal(x_T);
            const Real d_F = inner_prod(n_F, x_F-x_T);

            err_en += (m_F/d_F)*std::pow(Sol_maille[n](iT)-Sol_faces[n](iF)-sol.u(Temps[n],x_T)+sol.u(Temps[n],x_F),2);
            norm_u_h1 += (m_F/d_F)*std::pow(sol.u(Temps[n],x_T)-sol.u(Temps[n],x_F),2);
        }// for iF_loc
      } //if n < n-iter

    } // for iT
    err_uh_l2 = std::sqrt(err_uh_l2);
    norm_u_l2 = std::sqrt(norm_u_l2);
    err_tot_l2 = max(err_tot_l2,err_uh_l2);
    norm_linfty_l2 = max(norm_linfty_l2, norm_u_l2);
    err_l2_h1 += (Temps[n+1] -Temps[n]) * err_en;
    norm_l2_h1 += (Temps[n+1] -Temps[n]) * norm_u_h1;


  } // for n

  err_l2_h1 = std::sqrt(err_l2_h1);
  norm_l2_h1 = std::sqrt(norm_l2_h1);

  std::cout << FORMAT(50) << "les erreurs affichées sont des erreurs relatives"  << std::endl;
  std::cout << FORMAT(50) << "err_uh - L^infty _ t L²_ x : " << err_tot_l2 / norm_linfty_l2 << std::endl;
   std::cout << FORMAT(50) << "err_uh - L²_t H¹_x : " << err_l2_h1 / norm_l2_h1<< std::endl;

  Real m_vol = *min_element(Min_on_mesh.begin(), Min_on_mesh.end());
  Real m_edge = *min_element(Min_on_edge.begin(), Min_on_edge.end());
  std::cout << FORMAT(50) << "Minimum on mesh : " <<m_vol << std::endl;
  std::cout << FORMAT(50) << "Minimum on edge : " <<  m_edge << std::endl;
  std::cout << FORMAT(50) << "Number of linear resolutions: " <<  Nb_resol_sys_lin << std::endl;
  // std::cout << FORMAT(50) << "err_uh - L^infty _ t L²_ x : " << err_uh << std::endl;  
  std::cout << FORMAT(50) << "Time computation: " << c_time_cost.diff() << std::endl;



  vector<Real> Vec_data;
  Vec_data.push_back(h);
  Vec_data.push_back(err_tot_l2 / norm_linfty_l2);
  Vec_data.push_back(err_l2_h1 / norm_l2_h1 );
  Vec_data.push_back(m_vol);
  Vec_data.push_back(m_edge);
  Vec_data.push_back(0.);
  Vec_data.push_back(Nb_resol_sys_lin);

  std::cout << sep << std::endl;
  std::cout << FORMAT(50) << "DONE"  << std::endl;
  std::cout << sep << std::endl;

  return Vec_data;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

